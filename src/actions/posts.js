import * as type from './types';

export const get_post_list = () => {
    return {
        type: type.POSTS_REQUEST
    }
};
export const delete_post = (id) => {
    return {
        type: type.DELETE_POST,
        id
    }
};
export const update_post = (post) => {
    return {
        type: type.UPDATE_POST,
        post
    }
};
export const get_post_by_id = (id) => {
    return {
        type: type.GET_POST_BY_ID,
        id
    }
};
export const get_profile = () => {
    return {
        type: type.PROFILE,
    }
};