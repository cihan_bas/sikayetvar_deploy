import Index from '../components/ProfileDetailComponent';
import { get_profile} from "../actions/posts";
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        profile: state.profileReducer.profile,
        loader: state.profileReducer.loader,
        error: state.profileReducer.error,
    }

};
const mapStateToDispatch = (dispatch) => {
    return {
        get_profile: () => dispatch(get_profile()),

    }
};

export default connect(mapStateToProps, mapStateToDispatch)(Index);