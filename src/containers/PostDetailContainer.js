import Index from '../components/PostDetailComponent';
import {get_post_by_id} from "../actions/posts";
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        get_post_by_id_response: state.postsReducer.get_post_by_id_response,
        get_post_by_id_loader: state.postsReducer.get_post_by_id_loader,
        get_post_by_id_error: state.postsReducer.get_post_by_id_error,
    }

};
const mapStateToDispatch = (dispatch) => {
    return {
        get_post_by_id: (id) => dispatch(get_post_by_id(id)),

    }
};

export default connect(mapStateToProps, mapStateToDispatch)(Index);