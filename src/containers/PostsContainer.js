import Index from '../components/PostsComponent';
import {get_post_list, delete_post, update_post} from "../actions/posts";
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        posts: state.postsReducer.posts,
        post_loader: state.postsReducer.post_loader,
        error: state.postsReducer.error,
        delete_post_is_success: state.postsReducer.delete_post_is_success,
        update_post_is_success: state.postsReducer.update_post_is_success,
    }

};
const mapStateToDispatch = (dispatch) => {
    return {
        get_post_list: () => dispatch(get_post_list()),
        delete_post: (id) => dispatch(delete_post(id)),
        update_post: (post) => dispatch(update_post(post)),

    }
};

export default connect(mapStateToProps, mapStateToDispatch)(Index);