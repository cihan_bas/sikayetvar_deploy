import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import logo from './assets/img/logo.png';
import Posts from './containers/PostsContainer';
import PostDetail from './containers/PostDetailContainer';
import ProfileDerail from './containers/ProfileContainer';
import NotFound from './components/Notfound';

class Root extends Component {
    render() {
        return (
            <Router>
                <div className='header-wrapper bg'>
                    <div className="container">
                        <div className="row">
                            <div className='navbar valign-wrapper'>
                                <div className="col m3">
                                    <Link to="/">
                                        <img src={logo} alt=""/>
                                    </Link>
                                </div>
                                <div className="col m1">
                                    <Link to="/posts" className="posts">Posts</Link>
                                </div>
                                <div className="col m2 offset-m6">
                                    <button className="btn waves-effect waves-light white" type="submit" name="action">
                                        <Link to="/" className="login">Login</Link>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <img src="http://erikdkennedy.com/r-r-r-random/divider-triangle.png" className="triangle"/>
                    <div className='container renderbody'>
                        <div className="row">
                            <Switch>
                                <Route exact path="/" component={Posts}/>
                                <Route path="/post-detail/:id" component={PostDetail}/>
                                <Route path="/profile-detail/" component={ProfileDerail}/>
                                <Route component={NotFound}/>
                            </Switch>

                        </div>
                    </div>


                </div>

            </Router>
        )
    }
}

export default Root;