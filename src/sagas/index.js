import {call, all, takeEvery} from 'redux-saga/effects';
import * as method from "./postsSaga";
import * as type from "../actions/types/index";

function* watchPosts() {
    yield takeEvery(type.POSTS_REQUEST, method.getPosts)
}
function* watchdeletePost() {
    yield takeEvery(type.DELETE_POST, method.deletePost)
}

function* watchUpdatePost() {
    yield takeEvery(type.UPDATE_POST, method.updatePost)
}

function* watchGetPostByID() {
    yield takeEvery(type.GET_POST_BY_ID, method.getPostById)
}

function* watchGetProfile() {
    yield takeEvery(type.PROFILE, method.getProfile)
}

export default function* Sagas() {
    yield all([
            call(watchPosts),
            call(watchdeletePost),
            call(watchUpdatePost),
            call(watchGetPostByID),
            call(watchGetProfile),
        ]
    )
}
