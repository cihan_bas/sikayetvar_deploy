import api from '../api/api_methods';
import * as type from '../actions/types';
import {put} from 'redux-saga/effects';

function* getPosts() {
    try {
        const response = yield  api.posts();
        yield put({
            type: type.POSTS_REQUEST_SUCCESS,
            response
        })
    }
    catch (err) {
        yield put({
            type: type.POSTS_REQUEST_FAIL,
            error: err
        })
    }
}

function* deletePost(action) {
    try {
        const response = yield  api.deletePost(action.id);
        yield put({
            type: type.DELETE_POST_SUCCESS,
            response
        })
    }
    catch (err) {
        yield put({
            type: type.DELETE_POST_FAIL,
            error: err
        })
    }
}
function* updatePost(action) {
    try {
        const response = yield  api.updatePost(action.post);
        yield put({
            type: type.UPDATE_POST_SUCCESS,
            response
        })
    }
    catch (err) {
        yield put({
            type: type.UPDATE_POST_FAIL,
            error: err
        })
    }
}
function* getPostById(action) {
    try {
        const response = yield  api.getPostById(action.id);
        yield put({
            type: type.GET_POST_BY_ID_SUCCESS,
            response
        })
    }
    catch (err) {
        yield put({
            type: type.GET_POST_BY_ID_FAIL,
            error: err
        })
    }
}

function* getProfile() {
    try {
        const response = yield  api.getProfile();
        yield put({
            type: type.PROFILE_SUCCESS,
            response
        })
    }
    catch (err) {
        yield put({
            type: type.PROFILE_FAIL,
            error: err
        })
    }
}

export {
    getPosts,
    deletePost,
    updatePost,
    getPostById,
    getProfile
}