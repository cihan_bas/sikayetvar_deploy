import api from './index';

export default {
    posts() {
        return api.get('posts')
    },
    updatePost(post) {
        return api.put(`posts/${post.id}`, post)
    },
    deletePost(id) {
        return api.delete(`posts/${id}`)
    },
    getPostById(id) {
        return api.get(`posts/${id}`)
    },
    getProfile() {
        return api.get(`profile`)
    }
}
