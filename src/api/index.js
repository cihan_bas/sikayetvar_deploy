import axios from 'axios';

const host = 'http://localhost:3004/';
const axiosInstance = axios.create({
    baseURL: `${host}`
});
axiosInstance.defaults.headers['Content-Type'] = 'application/json';
axiosInstance.interceptors.response.use(response => {
    return response.data
}, error => {
    return Promise.reject(error)
});
export default axiosInstance;