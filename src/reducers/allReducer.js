import {combineReducers} from 'redux';
import postsReducer from './postsReducer';
import profileReducer from './profileReducer';

const allReducer = combineReducers({
    postsReducer,
    profileReducer
});

export default allReducer;