import * as type from "../actions/types";

const initialState = {
    posts: {},
    post_loader: true,
    error: null,
    delete_loader: false,
    update_loader: false,
    get_post_by_id_loader: false,
    delete_post: null,
    update_post: null,
    delete_error: null,
    update_error: null,
    get_post_by_id_error: null,
    delete_post_is_success: false,
    update_post_is_success: false,
    get_post_by_id_response: {}
};
const PostsReducer = (state = initialState, action) => {
    switch (action.type) {
        case type.POSTS_REQUEST:
            return {...state, post_loader: true, delete_post_is_success: false, update_post_is_success: false};

        case type.POSTS_REQUEST_SUCCESS:
            return {...state, posts: action.response, post_loader: false};
        case type.POSTS_REQUEST_FAIL:
            return {...state, error: action.error, post_loader: false};

        case type.DELETE_POST:
            return {...state, delete_loader: true};
        case type.DELETE_POST_SUCCESS:
            return {...state, delete_loader: false, delete_post: action.response, delete_post_is_success: true};
        case type.DELETE_POST_FAIL:
            return {...state, delete_loader: false, delete_error: action.error};

        case type.UPDATE_POST:
            return {...state, update_loader: true};
        case type.UPDATE_POST_SUCCESS:
            return {...state, update_loader: false, update_post: action.response, update_post_is_success: true};
        case type.UPDATE_POST_FAIL:
            return {...state, update_loader: false, update_error: action.error};

        case type.GET_POST_BY_ID:
            return {...state, get_post_by_id_loader: true};
        case type.GET_POST_BY_ID_SUCCESS:
            return {...state, get_post_by_id_loader: false, get_post_by_id_response: action.response,};
        case type.GET_POST_BY_ID_FAIL:
            return {...state, get_post_by_id_loader: false, get_post_by_id_error: action.error};
        default:
            return state;
    }
};

export default PostsReducer;