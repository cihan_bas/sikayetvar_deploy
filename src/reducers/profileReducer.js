import * as type from "../actions/types";

const initialState = {
    profile: {},
    loader: true,
    error: null,
};
const ProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case type.PROFILE:
            return {...state, loader: true};
        case type.PROFILE_SUCCESS:
            return {...state, loader: false, profile: action.response};
        case type.PROFILE_FAIL:
            return {...state, error: action.error, loader: false};

        default:
            return state;
    }
};

export default ProfileReducer;