import React, {Component} from 'react';
import Router from './Router'
import {Provider} from 'react-redux';
import allReducer from './reducers/allReducer';
import {createStore, applyMiddleware, compose} from 'redux';
import {createLogger} from 'redux-logger';
import rootSaga from './sagas';
import middleaware from 'redux-saga';

export const sagaMiddleware = middleaware();
const store = createStore(allReducer, compose(applyMiddleware(sagaMiddleware, createLogger())));

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router/>
            </Provider>
        );
    }
}

sagaMiddleware.run(rootSaga);
export default App;
