import React, {Component} from 'react';
import Modal from 'react-responsive-modal';
import {Link} from 'react-router-dom';

class Posts extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            posts: {},
            error: null,
            title: '',
            body: '',
            id: null,
            open: false,
            delete_modal_state: false

        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.props.get_post_list();
    }

    open(post) {
        this.setState({...post})
    }

    onOpenModal(post) {
        this.setState({open: true, ...post});
    }

    onCloseModal() {
        this.setState({open: false});
    }

    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value

        });
    }

    openDeleteModal(post) {
        // this.props.delete_post(id);
        this.setState({delete_modal_state: true, ...post});


    }

    onDeletePost() {
        this.props.delete_post(this.state.id);
        this.closeDeleteModal();
    }

    updatePost() {
        const post = {
            id: this.state.id,
            title: this.state.title,
            body: this.state.body,
        };
        this.props.update_post(post);
        this.onCloseModal()
    }

    closeDeleteModal() {
        this.setState({delete_modal_state: false});
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.delete_post_is_success || nextProps.update_post_is_success) {
            this.props.get_post_list();
        }
    }

    render() {
        return (
            <div>
                <ul className="postlist-wrapper row">
                    {this.props.posts.length && this.props.posts.map((item, idx) => {
                        return (
                            <li className="valign-wrapper postlist-item col m12" key={idx}>

                                <p className="col s12 m8">
                                    {item.title}
                                </p>
                                <div className="col m4">
                                    <Link to={`/post-detail/${item.id}`}
                                          query={item}
                                          className="waves-effect waves-light btn  blue accent-3"
                                           onClick={() => this.open()}> Detay
                                    </Link>
                                    <button className="waves-effect waves-light btn  green accent-3 modal-trigger"
                                            onClick={() => this.onOpenModal(item)}> Düzenle
                                    </button>
                                    <button className="waves-effect waves-light btn  red accent-3"
                                            onClick={() => this.openDeleteModal(item)}> Sil
                                    </button>
                                </div>
                            </li>
                        )
                    }
                    )}
                </ul>
                <Modal open={this.state.open} onClose={() => this.onCloseModal()}>
                    <div className="modal-content">
                        <div className="row">
                            <h5 className="col m11">Düzenle</h5>
                        </div>
                        <div className='divider'/>
                        <div className="modal-body">
                            <div className="input-field">
                                <p className='title'>Title</p>
                                <input value={this.state.title} type="text" name="title"
                                       onChange={this.handleChange}/>
                            </div>
                            <div className="input-field">
                                <p className='title'>Body</p>
                                <textarea value={this.state.body} onChange={this.handleChange} rows={5} name="body"/>
                            </div>
                            <div className="divider"/>
                            <div className="modal-footer left">
                                <a className="secondary-content waves-effect waves-light btn  col s4 m3 cyan accent-3"
                                   href="#" onClick={() => this.updatePost()}> Güncelle</a>
                            </div>
                        </div>
                    </div>
                </Modal>

                <Modal open={this.state.delete_modal_state} onClose={() => this.closeDeleteModal()}>
                    <div className="modal-content">
                        <div className="row">
                            <h5 className="col m11">Uyarı </h5>
                        </div>
                        <div className='divider'/>
                        <div className="modal-body">
                            <div className="input-field">
                                <p>
                                    <b>
                                        {this.state.title}
                                    </b> <br/>
                                    postunu silmek istediğinize emin misiniz</p>

                            </div>

                            <div className="divider"/>
                            <div className="modal-footer left">
                                <button
                                    className="secondary-content waves-effect waves-light btn  col s4 m3 cyan accent-3"
                                    onClick={() => this.onDeletePost()}> EVET
                                </button>
                            </div>
                            <div className="modal-footer left">
                                <button
                                    className="secondary-content waves-effect waves-light btn  col s4 m3 cyan accent-3"
                                    onClick={() => this.closeDeleteModal()}> HAYIR
                                </button>
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        )
    }
}

export default Posts;
