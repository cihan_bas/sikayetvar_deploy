import React, {Component} from 'react';
import {Spinner} from "./Loader";
import {Link} from 'react-router-dom';

class PostDetailComponent extends Component {
    state = {
        post: null,
        loader: true,
        error: null,
        id: null
    };

    componentDidMount() {
        const {id} = this.props.match.params;
        this.setState({
            id
        })
        this.props.get_post_by_id(id);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            post: nextProps.get_post_by_id_response,
            loader: nextProps.get_post_by_id_loader,
            error: nextProps.get_post_by_id_error,
        })
    }

    renderDetail() {
        return (
            <div className="row">

                <div className="section">
                    <h5>  {this.state.post.title}</h5>
                    <p>  {this.state.post.body}</p>
                </div>
                <Link to='/profile-detail/'>{this.state.post.author}</Link>
            </div>
        )
    }

    render() {
        console.log('state', this)
        return (

            <div>
                {this.state.loader ?

                    <Spinner/> :
                    this.state.error ?
                        <div>{this.state.id} id'sine sahip post Bulunamadı </div>
                        :
                        this.renderDetail()
                }
            </div>
        )
    }
}

export default PostDetailComponent;
