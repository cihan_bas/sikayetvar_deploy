import React, {Component} from 'react';
import {Spinner} from "./Loader";
import {Link} from 'react-router-dom';

class PostDetailComponent extends Component {
    state = {
        post: null,
        loader: true,
        error: null,
        id: null
    };

    componentDidMount() {
        const {id} = this.props.match.params;
        this.setState({id});
        this.props.get_profile();
    }
    render() {
        return (

            <div className="row">
                <div className="col m6">
                    <div className="section">
                        <h5 className='capitalize'>  {this.props.profile.name}</h5>
                        <p>  {this.props.profile.country}</p>
                    </div>
                    <div className="col m6">
                       <p>Username </p>

                    </div>
                    <div className="col m6">
                        <p>{this.props.profile.userName} </p>

                    </div>
                </div>
            </div>
        )
    }
}

export default PostDetailComponent;
